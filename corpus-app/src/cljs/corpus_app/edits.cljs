(ns corpus-app.edits
  (:require
   [corpus-app.db :as db]))

(defn insert
  ""
  [utterance previous items]
  (if (= db/head previous)
    (into []
      (concat items utterance))
    (reduce (fn [acc item] (if (= item previous)
                             (into [] (concat acc [item] items))
                             (conj acc item)))
      []
      utterance)))

(defn substitute
  [utterance targets replacements]
  (loop [utterance utterance
         acc []]
    (let [n (count targets)
          prefix (take n utterance)]
      (cond
        (empty? utterance) acc
        (= targets prefix) (recur
                             (drop n utterance)
                             (into [] (concat acc replacements)))
        :else (recur
                (rest utterance)
                (into [] (conj acc (first utterance))))))))

(defmulti make-editor :type)
(defmethod make-editor :sub [{:keys [target replacement]}]
  (fn [utt] (substitute utt target replacement)))
(defmethod make-editor :ins [{:keys [target replacement]}]
  (fn [utt] (insert utt (first target) replacement)))

(defn apply-edits [edit-list words]
  (let [editor (->> edit-list
                 reverse (map make-editor) (apply comp))]
    (editor words)))

(defn anonymize [words]
  (map #(dissoc % :uuid) words))

(defmulti content?
  "Returns true if an edit actually has content to be saved"
  :name)

(defmethod content? :default [edit] false)

(defmethod content? :edit
  [edit]
  (when-let [{:keys [target replacement]} edit]
    (not=
      (anonymize target)
      (anonymize replacement))))

(defmethod content? :split
  [edit]
  (when-let [{:keys [target replacement]} edit]
    (let [target (anonymize target)
          replacement (anonymize replacement)
          empty (dissoc (db/empty-word) :uuid)]
      (not= replacement [(first target) empty]))))

(defmethod content? :insert
  [edit]
  (not=
    (anonymize [(db/empty-word)])
    (anonymize (:replacement edit))))

(defmethod content? :combine
  [edit]
  (not=
    (anonymize [(db/empty-word)])
    (anonymize (:replacement edit))))
