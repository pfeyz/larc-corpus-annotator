(ns corpus-app.utils
  (:require [re-frame.core :as re-frame]))

(def pad #(str (when (< % 10) "0") %))

(defn format-timestamp [date]
  (let [min (.getMinutes date)
        hour (-> date .getHours (mod 12))]
    (clojure.string/join ":" (map pad [hour min]))))

(defn glyph [name]
  [:span {:class-name (str "glyphicon glyphicon-" name)}])

(defn click-dispatch [v]
  (fn [e]
    (.preventDefault e)
    (re-frame/dispatch v)))

(defn get-attrs [[head & xs :as body]]
  (if (map? head)
    [head xs]
    [{} body]))

(defn node-with-defaults [node-type defaults body]
  (let [[attrs body] (get-attrs body)
        attrs (merge-with #(str %1 " " %2) defaults attrs)]
    (into []
      (concat [node-type attrs] body))))

(defn col [width & body]
  (node-with-defaults :div {:class-name (str "col col-xs-" width)} body))

(defn row [& body]
  (node-with-defaults :div {:class-name "row"} body))
