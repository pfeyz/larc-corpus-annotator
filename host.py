import os, json, uuid, bcrypt
from flask import (Flask, render_template, request, send_from_directory,
                   session, g, redirect, url_for, abort)
from wtforms import Form, IntegerField, TextField, validators
from schema import app, db, Corpus, File, Utterance, User, Correction

'''Front-end routes'''

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/main.js')
def main_js():
    return app.send_static_file('main.js')

@app.route('/css/<path:path>')
def css(path):
    return send_from_directory('corpus-app/resources/public/css', path)

@app.route('/js/<path:path>')
def js(path):
    return send_from_directory('corpus-app/resources/public/js', path)

@app.route('/fonts/<path:path>')
def fonts(path):
    return send_from_directory('corpus-app/resources/public/fonts', path)

class AlreadyLoggedIn(Exception):
    pass

class UsernameNotFound(Exception):
    pass

class BadPassword(Exception):
    pass

def authenticate_user(username, password):
    current_user = session.get('user')
    if current_user:
        raise AlreadyLoggedIn()

    try:
        user = User.query.filter(User.name == username).one()
    except sqlalchemy.orm.exc.NoResultFound:
        raise UsernameNotFound()

    hashed = bcrypt.hashpw(bytes(password), bytes(user.hashedword))
    if hashed != bytes(user.hashedword):
        raise BadPassword()
    session['user'] = user.name

@app.route('/api-login', methods=['POST'])
def api_login():
    try:
        authenticate_user(request.form.get('username'),
                          request.form.get('password'))
    except AlreadyLoggedIn:
        return json_report('success', None)
    except UsernameNotFound:
        return json_report('failure', {'cause': 'username'})
    except BadPassword:
        return json_report('failure', {'cause': 'password'})
    return json_report('success', None)


'''Major back-end routes'''
@app.route('/login', methods=['GET', 'POST'])
def login():
    """Allows a User to login.
    """
    if session.get('user'):
        return "You are already logged in as {}".format(session['user'])
    form = LoginForm(request.form)
    message = None
    if request.method == 'POST':
        users = User.query
        try:
            this_user = users.filter(User.name == request.form['username']).one()
        except:
            return render_template('login.html',
                                   message='Invalid Username',
                                   form=form)
        if bcrypt.hashpw(bytes(request.form['password']),
                        bytes(this_user.hashedword)) != bytes(this_user.hashedword):
            return render_template('login.html',
                                   message='Invalid Password',
                                   form=form)
        session['user'] = this_user.name
        if request.args.get('action', "") == 'correction':
            return redirect('/correction')
        return redirect('/')
    if request.args.get('action', "") == 'correction':
        message = "You haven't logged in yet"
    elif request.args.get('action', "") == 'logout':
        message = "You are now logged out"
    return render_template('login.html', message=message, form=form)

@app.route('/logout')
def logout():
    """Allows a User to logout.
    """
    session.pop('user', None)
    return redirect('/login?action=logout')

@app.route('/new-user', methods=['GET', 'POST'])
def create_user():
    """For new User creation. Doesn't need any preexisting credentials yet.
    """
    form = LoginForm(request.form)
    message = "Enter the new account's credentials"

    if request.method == 'POST':
        if len(request.form['username']) < 3 or len(request.form['password']) < 3:
            return render_template('login.html',
                                   message='User credentials must be 3 or more characters long',
                                   form=form)
        users = [user.name
                 for user in User.query.all()]
        if request.form['username'] in users:
            return render_template('login.html',
                                   message='That username is already taken',
                                   form=form)
        else:
            new_user = User(name=request.form['username'],
                            hashedword=bcrypt.hashpw(bytes(request.form['password']), bcrypt.gensalt()))
            db.session.add(new_user)
            db.session.commit()
        session['user'] = request.form['username']
        return redirect('/')
    return render_template('login.html', message=message, form=form)

@app.route('/correction/next')
def next_unchecked():
    """Returns the next Utterance in the database without any associated Corrections.
    For a specific User, may return Utterances edited by other Users.
    """
    uc = db.session.query(Corpus.name.label('cname'),
                         File.name.label('fname'),
                         Utterance.uid.label('uid'),
                         Utterance.id.label('pk'),
                         Correction.user_id.label('editorid'))
    uc = uc.filter(Corpus.id == File.corpus_id,
                   File.id == Utterance.file_id)
    uc = uc.outerjoin(Correction, Utterance.id == Correction.utterance_id)
    uc = uc.filter(Correction.user_id == None)
    uc = uc.order_by(Corpus.name, File.name, Utterance.uid)
    uc = uc.first()
    utt = Utterance.query.filter(Utterance.id == uc.pk).one_or_none()
    dumped_utt = json.loads(utt.to_json())
    if session.get('user'):
        start_correction(utt)  # due to db.session.commit, utt is now wiped
    return json_report('success', dumped_utt)

@app.route('/correction', methods=['POST', 'GET'])
def post_correction():
    """Displays a form for accepting Corrections to the database.
    Calls helper methods assess_correction and abjure_correction.
    Returns a report describing success or failure and any errors.
    """
    if not session.get('user'):
        return redirect('/login?action=correction')
    error = None
    unftext = "UNF; invalid {}"
    eivtext = "EIV; {}"
    form = CorrectionForm(request.form)

    if request.method == 'POST':
        try:
            utt = get_utt_by_pk(request.form['pk'])
        except UtteranceNotFound as unf:
            return json_report('failure', unftext.format(unf.error))

        editlist = json.loads(request.form['edit'])
        try:
            edited = apply_editlist(editlist, json.loads(utt.words))
        except EditInvalid as eiv:
            return json_report('failure', eivtext.format(eiv.message))

        save_correction(editlist=request.form['edit'],
                        utterance=utt,
                        user=this_user())
        return json_report('success', edited)

    return render_template('correction.html', error=error, form=form)


'''Major helper methods'''

def apply_editlist(editlist, words):
    """Assesses edits. If OK, returns an edited list of the words.
    """
    words.insert(0, {"word": "",
                     "suffix": [],
                     "prefix": [],
                     "subPos": [],
                     "pos": "",
                     "fusion": [],
                     "stem": "",
                     "uuid": 'head'})
    for edit in editlist:
        assess_edit(edit, words)
        words = apply_edit(edit, words)
    words.pop(0)
    return words

def apply_edit(edit, words):
    """Applys a single edit to a list of words. Returns the result.
    """
    targinds = []
    for i in range(len(words)):
        if words[i] in edit['target']:
            targinds.append(i)
    targinds.sort()
    '''will there ever be a situation where we need disconnected selects?'''
    cut1 = targinds[0] + 1
    if edit['type'] == 'ins':
        corrected = words[:cut1] + edit['replacement'] + words[cut1:]
    elif edit['type'] == 'sub':
        cut1 -= 1
        cut2 = targinds[len(targinds) - 1] + 1
        corrected = words[:cut1] + edit['replacement'] + words[cut2:]
    return corrected

def assess_edit(edit, words):
    """Assesses an edit for validity.
    If anything goes wrong, raises an EditInvalid exception.
    If it runs to completion, returns a message describing success.
    """
    for key in ['type', 'target', 'replacement']:
        try:
            edit[key]
        except:
            raise EditInvalid('edit missing key [{}]'.format(key))
    # validate words
    for i in range(len(edit.get('replacement', []))):
        for key in ['word', 'prefix', 'pos', 'subPos', 'stem', 'fusion', 'suffix', 'uuid']:
            try:
                edit.get('replacement', [])[i][key]
            except:
                raise EditInvalid('word missing key [{}]'.format(key))
        for key in ['uuid', 'word', 'pos', 'stem']:
            if len(edit.get('replacement', [])[i][key]) < 1:
                raise EditInvalid('word key [{}] blank'.format(key))
    # validate target
    if len(edit['target']) <= 0:
        raise EditInvalid('not targeted')
    found_targets = [word
                     for word in words
                     if word in edit['target']]
    if len(found_targets) < len(edit['target']):
        raise EditInvalid('target not found')
    # validate type
    types = ['ins', 'sub']
    if edit['type'] not in types:
        raise EditInvalid('type not defined')
    # print success message
    # message = "successful {}!".format(edit['type'])
    # return json_report('success', message)

def save_correction(editlist, utterance, user):
    """Updates the open ('lock') Correction associated with this utterance and user.
    Inserts the editlist, sets accepted to True, and updates the timestamp.
    """
    this_edit = get_corrections(utterance).filter(Correction.accepted == False,
                                            Correction.user == user)
    if len(this_edit.all()) < 1:
        start_correction(utterance, editlist=editlist, accepted=True)
    else:
        this_edit.update({Correction.editlist: editlist,
                          Correction.accepted: True,
                          Correction.timestamp: func.now()},
                         synchronize_session='fetch')
    db.session.commit()

def start_correction(utterance, editlist='', accepted=False):
    """Creates an Correction in the database.
    Normally used to 'lock' an utterance. This creates an Correction with an
    empty editlist and accepted set to False.
    Can be used to create an immediately accepted Correction if necssary.
    """
    new_edit = Correction(editlist=editlist,
                    utterance=utterance,
                    user=this_user(),
                    accepted=accepted)
    db.session.add(new_edit)
    db.session.commit()

def get_corrections(utterance):
    """Returns a query containing all Edits associated with a given Utterance.
    """
    return Correction.query.filter(Correction.utterance == utterance)


'''Minor back-end routes'''

@app.route('/api/corpora/')
def json_corpora():
    """should return a list of all the corpus names
    (we just have valian right now).
    """
    corpora = get_corpora().all()
    return json.dumps([corpus.name
                      for corpus in corpora])

@app.route('/api/corpora/<corpus>/')
def json_files(corpus):
    """should return a list of all the files in <corpus>
    (where <corpus> we retrieved via that /corpora/ route)
    """
    files = get_files(corpus).all()
    return json.dumps([file.name
                       for file in files])

@app.route('/api/corpora/<corpus>/<filename>/')
def json_utts(corpus, filename):
    """should return all the utterances in filename,
    and should handle start and limit query params (or whatever we named start)

    Returns a number of utterances from a given file,
    based on the request query. Defaults to all utterances.
    """
    utts = get_utterances(corpus, filename).all()
    return json_translate(utts, request)

@app.route('/api/corpora/<corpus>/<filename>/<int:uid>')
def json_utt(corpus, filename, uid):
    """should return a single utterance

    Returns a single utterance by filename and utterance id.
    """
    utts = [get_utterance(corpus, filename, uid)]
    return json_translate(utts)

@app.route('/api/<pk>')
def pk_utt(pk):
    """Returns a single utterance by primary key.
    """
    utts = [get_utt_by_pk(pk)]
    return json_translate(utts)

def get_neighbors(utt_pk):
    """ Returns the previous and next utterance's pks, if any. """
    conn = engine.connect()
    res = conn.execute("select rowid from ordered_utts where ordered_utts.id = ?", utt_pk).fetchone()
    if res is None:
        return []
    rowid = res['rowid']
    prev_id = conn.execute("select id from ordered_utts where rowid = ?", rowid - 1).fetchone()
    next_id = conn.execute("select id from ordered_utts where rowid = ?", rowid + 1).fetchone()
    return [prev_id['id'] if prev_id else None,
            next_id['id'] if next_id else None]

@app.route('/api/utterance/neighbors')
def utterance_neighbors():
    prev_id, next_id = get_neighbors(int(request.args['id']))
    return json_report('success', {"prev": prev_id, "next": next_id})

@app.route('/api/utterance/<pk>')
def utt_plus(pk):
    edits = get_edits(pk).filter(Correction.accepted == True,
                                 Correction.user_id == this_user().id).all()
    edits = [json.loads(e.to_json()) for e in edits]
    d = {'utterance': json.loads(get_utt_by_pk(pk).to_json()),
         'edits': edits}

    return json_report('success', d)


'''Convenience methods'''

def get_edits(pk):
    """Returns a query containing all edits by utterance pk and user."""
    return Correction.query.filter(Correction.utterance_id == pk)

def get_corpora():
    """Returns a query containing all corpora in the database.
    """
    return Corpus.query

def get_corpus(name):
    """Returns a single corpus by name.
    """
    corpus = get_corpora().filter(Corpus.name == name).one_or_none()
    if corpus is None:
        raise UtteranceNotFound('corpus name')
    else:
        return corpus

def get_files(corpus_name):
    """Returns a query containing all the files in a given corpus.
    """
    return File.query.filter(File.corpus_id == get_corpus(corpus_name).id)

def get_file(corpus_name, fname):
    """Returns a single file by name and corpus.
    """
    xmlfile = get_files(corpus_name).filter(File.name == fname).one_or_none()
    if xmlfile is None:
        raise UtteranceNotFound('filename')
    else:
        return xmlfile

def get_utterances(corpus_name, fname):
    """Returns a query containing all utterances in a given file and corpus.
    """
    return Utterance.query.filter(Utterance.file_id == get_file(corpus_name, fname).id)

def get_utterance(corpus_name, fname, uid):
    """Returns a single utterance by corpus, name, and utterance id.
    """
    utterance = get_utterances(corpus_name, fname).filter(Utterance.uid == uid).one_or_none()
    if utterance is None:
        raise UtteranceNotFound('utterance id')
    else:
        return utterance

def get_utt_by_pk(pk):
    """Returns a single utterance by primary key.
    """
    utterance = Utterance.query.filter(Utterance.id == pk).one_or_none()
    if utterance is None:
        raise UtteranceNotFound('primary key')
    else:
        return utterance

def json_report(status, message, **kwargs):
    """Returns a report on the status of a correction as a json string.
    Contains a dict with status, user, and message,
    as well as any other custom keys specified in the call.
    """
    report = {'status': status, 'message': message}
    if session.get('user'):
        report['user'] = dict(username=session['user'])
    for key in kwargs:
        report[key] = kwargs[key]
    return json.dumps(report)

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (Corpus, File, Utterance, Correction, User)):
        return json.loads(obj.to_json())
    raise TypeError ("Type not serializable")

def json_translate(jsons, req=None):
    """Based on a request, may start the list at start_id,
    and may cut the list to limit number of utterances after start_id.
    Defaults to translating the entire list.
    """
    start = 0
    lim = len(jsons)
    if req is not None:
        start = int(req.args.get('start_id', 0))
        lim = int(req.args.get('limit', len(jsons) - start))

    d = [json.loads(jsons[i].to_json())
        for i in range(start, start + lim)]

    return json.dumps(d)

def this_user():
    """Returns the User currently in session."""
    return User.query.filter(User.name == session['user']).one_or_none()












'''Class definitions!
Pretty much for Forms and Exceptions'''

class CorrectionForm(Form):
    """Defines the correction form used in /correction
    Fields take a primary key (of the utterance concerned)
    and a json string describing a list of edits.
    """
    pk = TextField('Primary Key')
    edit = TextField('Correction')

class LoginForm(Form):
    """Defines the login form used in /login
    """
    username = TextField('Username')
    password = TextField('Password')


class EditInvalid(Exception):
    """Should be raised if there is an error in processing an edit.
    Carries a 'message' describing the problem.
    """
    def __init__(self, message):
        Exception.__init__(self, message)
        self.message = message

class UtteranceNotFound(Exception):
    """Should be raised if there is an error in accessing the database.
    If the corpus name, filename, utterance id, or utterance primary key
    doesn't show up in a search, it will appear.
    Can carry an 'error' describing the problematic input.
    """
    def __init__(self, error):
        Exception.__init__(self, error)
        self.error = error


# main
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


