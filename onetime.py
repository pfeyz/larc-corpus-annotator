import os

from talkbank_parser import parse_xml
from glob import glob

def old_xml():
    fpaths = glob('tagger-evaluation-docs-dropbox_xml-converted-from-cha/*.xml')
    if not os.path.exists('xml-txt-unchanged'):
        os.makedirs('xml-txt-unchanged')
    for fpath in fpaths:
        parse_xml(fpath, "xml-txt-unchanged/{}.txt".format(os.path.basename(fpath)))

def dump_sql():
    '''This function requires that sqlite3 be installed on your computer.
    
    After running this function, the dumpfile must be post-processed.
    
    Remove the following line from the beginning of the document:
    
    [PRAGMA foreign_keys=OFF;]
    
    Use the find-replace tool. MAKE SURE to select option "Whole word only".
    Perform the following replacements:
    
    [ user ]    --> [ "user" ]
    [valian]    --> [tagger-evaluation-docs-dropbox_xml-converted-from-cha]
    [DATETIME]  --> [TIMESTAMP]
    [BOOLEAN]   --> [INTEGER]
    '''
    os.system("sqlite3 data.db .dump > dumpfile.sql")

if __name__ == '__main__':
    # old_xml()
    dump_sql()
    pass