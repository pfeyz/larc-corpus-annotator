import os, json, sys
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder="corpus-app/resources/public")
app.config.from_object(__name__)

user = os.getenv('CORPUS_APP_DB_USER')
pswd = os.getenv('CORPUS_APP_DB_PASSWORD')
dbnm = os.getenv('CORPUS_APP_DB_NAME')

if user is None or pswd is None or dbnm is None:
    print os.environ.keys()
    errmsg = "Please setup environmental variables for all of the following:"
    if user is None:
        errmsg += " CORPUS_APP_DB_USER"
    if pswd is None:
        errmsg += " CORPUS_APP_DB_PASSWORD"
    if dbnm is None:
        errmsg += " CORPUS_APP_DB_NAME"
    sys.exit(errmsg)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://{}:{}@localhost/{}'.format(
                                os.getenv('CORPUS_APP_DB_USER'),
                                os.getenv('CORPUS_APP_DB_PASSWORD'),
                                os.getenv('CORPUS_APP_DB_NAME'))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'test.db'),
    SECRET_KEY='larc tle lerahd angehs sing'
))
db = SQLAlchemy(app)


class Serializable(object):
    def to_dict(self):
        raise NotImplementedError()

    def to_json(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        return self.to_dict() == other.to_dict()


class Corpus(db.Model, Serializable):
    __tablename__ = 'corpus'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    label = db.Column(db.Text)

    files = db.relationship("File", back_populates="corpus")

    def to_dict(self):
        jdict = self.__dict__.copy()
        jdict.pop('_sa_instance_state', None)
        return jdict

class File(db.Model, Serializable):
    __tablename__ = 'file'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, default=db.func.now())
    hash = db.Column(db.Text)
    path = db.Column(db.Text)
    corpus_id = db.Column(db.Integer, db.ForeignKey('corpus.id'))

    corpus = db.relationship("Corpus", back_populates="files")
    utterances = db.relationship('Utterance', back_populates='file')

    def to_dict(self):
        jdict = self.__dict__.copy()
        jdict.pop('_sa_instance_state', None)
        jdict.pop('corpus', None)
        jdict['corpusname'] = self.corpus.name
        jdict['timestamp'] = jdict['timestamp'].isoformat()
        return jdict

class Utterance(db.Model, Serializable):
    __tablename__ = 'utterance'
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer)
    speaker = db.Column(db.Text)
    words = db.Column(db.Text)
    file_id = db.Column(db.Integer, db.ForeignKey('file.id'))

    file = db.relationship("File", back_populates='utterances')
    corrections = db.relationship("Correction", back_populates='utterance')

    def to_dict(self):
        jdict = self.__dict__.copy()
        jdict.pop('_sa_instance_state', None)
        jdict.pop('file', None)
        jdict['words'] = json.loads(jdict['words'])
        jdict['filename'] = self.file.name
        jdict['corpusname'] = self.file.corpus.name
        return jdict

class User(db.Model, Serializable):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True)
    hashedword = db.Column(db.Text)

    corrections = db.relationship("Correction", back_populates='user')

    def to_dict(self):
        jdict = self.__dict__.copy()
        jdict.pop('_sa_instance_state', None)
        jdict.pop('hashedword', None)
        return jdict

class Correction(db.Model, Serializable):
    __tablename__ = 'correction'
    id = db.Column(db.Integer, primary_key=True)
    editlist = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, default=db.func.now())
    accepted = db.Column(db.Boolean)
    utterance_id = db.Column(db.Integer, db.ForeignKey('utterance.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    utterance = db.relationship('Utterance', back_populates='corrections')
    user = db.relationship('User', back_populates='corrections')

    def to_dict(self):
        jdict = self.__dict__.copy()
        jdict.pop('_sa_instance_state', None)
        jdict.pop('utterance', None)
        jdict.pop('user', None)
        jdict['editlist'] = json.loads(jdict['editlist'])
        jdict['username'] = self.user.name
        jdict['timestamp'] = jdict['timestamp'].isoformat()
        return jdict

if __name__ == '__main__':
    db.create_all()
